'use strict';

var express = require('express');
var bodyparser = require('body-parser');
var app = express();

app.use(bodyparser.json());
app.use(express.static(__dirname + '/'));

var basicAuth = require('basic-auth');

app.use(function(req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.sendStatus(401);
  };

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  };

  if (user.name === 'tving' && user.pass === 'tving###') {
    return next();
  } else {
    return unauthorized(res);
  };
});

app.get('/', function (req, res) {
 console.log('Hello World!' + __dirname);
 res.sendFile(__dirname + '/index2.html');
});

app.get('/admin', function (req, res) {
  res.redirect('/admin.html');
});

var
  dbAddr = 'ocdb.angry-octopus.com',
  port = 2012;

var mdb = require('mongodb'),
  mdbc = mdb.MongoClient,
  mdbServer = mdb.Server(dbAddr, port),
  dbHandle = mdb.Db('tvrelease', mdbServer, { safe: true }),
  initDb;


initDb = function() {
  return new Promise(function(resolve, reject) {
    mdbc.connect('mongodb://' + dbAddr + ':' + port + '/tvrelease', function(err, db) {
      db.listCollections().toArray(function(err, items) {
        db.createCollection('rel', {
        }).catch(function(err) {
          console.log(err.toString());
          reject(err);
        }).then(function(collection) {
          resolve(db);
        });
      });
    });
  });
}

initDb().then(function(db) {
  console.log('connected DB');
  app.route('/build')
  .get(function(req, res) {
    console.log("get build");
    db.collection('rel').find().sort({_id:-1}).limit(100).toArray().then(function(docs) {
    res.send(docs);
  });
  })
  .post(function(req, res) {
    
    var opMap = {safe: true};
    var objMap = req.body;
    console.log(objMap);
    db.collection('rel').insert(objMap, opMap).then(function(resultmap) {
    res.send('Add a build');                    
                    }).catch(function(ierr) {
                        console.log(ierr);
                        res.send(ierr);
                    });
  })
  .put(function(req, res) {
    res.send('Update the build');
  });
  

  app.route('/build/:id')
  .delete(function(req, res) {
    var oid = mdb.ObjectID(req.params.id);
    db.collection('rel').deleteOne({_id: oid}).then(function(result) {
                    res.send('Delete the build' + result);
                }).catch(function(err) {
                    res.send('fffff Delete the build' + req.param('id'));
                });

  });
});

app.listen(2058, function () {
  console.log('listening on port 2058!');
});